package com.example.screamitus_android;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    Infection infection;

    @Before
    public void setup(){

        infection = new Infection();

    }


    @Test
    public void checkDaysTC1(){

        int numInfected = infection.calculateTotalInfected(0);
        assertEquals(-1, numInfected);


    }

    @Test
    public void infection5instTC2(){

        int numInfected = infection.calculateTotalInfected(4);
        assertEquals(20, numInfected);

    }
    @Test
    public void TC3(){
        int numInfected = infection.calculateTotalInfected(9);
        assertEquals(51, numInfected);
    }

    @Test
    public void TC4(){
        int numInfected = infection.calculateTotalInfected(9);
        assertEquals(28, numInfected);
    }

}